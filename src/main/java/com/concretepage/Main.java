package com.concretepage;
import com.concretepage.entity.Dostawa;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.util.Date;

public class Main extends Application {
    public static void main(String[] args) {
/*
        EntityManager entityManager = JPAUtility.getEntityManager();
        entityManager.getTransaction().begin();
        Dostawa employee = new Dostawa(3,"ktos",new Date(),2,"kabel",15);
        entityManager.persist(employee);
        entityManager.getTransaction().commit();
        entityManager.close();
        JPAUtility.close();
*/
        //launch(args);
        System.out.println("Entity saved.");
    }


    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("fxml/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}