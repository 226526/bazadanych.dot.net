package com.concretepage.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="Dostawa")
public class Dostawa {
    @Id
    @Column(name="Id")
    private int id;

    @Column(name="zarejestrował")
    private String zarejestrował;

    @Column(name="data")
    private Date data;

    @Column(name="kwota")
    private int kwota;

    @Column(name="produkt")
    private String produkt;

    @Column(name="ilosc_produkt")
    private int ilosc_produkt;

    public Dostawa(int id, String zarejestrował, Date data, int kwota, String produkt, int ilosc_produkt) {
        this.id = id;
        this.zarejestrował = zarejestrował;
        this.data = data;
        this.kwota = kwota;
        this.produkt = produkt;
        this.ilosc_produkt = ilosc_produkt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZarejestrował() {
        return zarejestrował;
    }

    public void setZarejestrował(String zarejestrował) {
        this.zarejestrował = zarejestrował;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getKwota() {
        return kwota;
    }

    public void setKwota(int kwota) {
        this.kwota = kwota;
    }

    public String getProdukt() {
        return produkt;
    }

    public void setProdukt(String produkt) {
        this.produkt = produkt;
    }

    public int getIlosc_produkt() {
        return ilosc_produkt;
    }

    public void setIlosc_produkt(int ilosc_produkt) {
        this.ilosc_produkt = ilosc_produkt;
    }
}
